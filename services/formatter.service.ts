import { basename } from 'path';

export class FormatterService {
  private static hasKeyToFormat(keysToFormat: string[], content: object): boolean {
    const contentKeys = Object.keys(content);
    return keysToFormat.some((keyToFormat) => contentKeys.includes(keyToFormat));
  }

  public static formatDate(dateString: string): string {
    const date = new Date(dateString);
    const localeDate = date.toLocaleDateString();
    return localeDate;
  }

  public static formatData<T>(context: __WebpackModuleApi.RequireContext, resource: string): T[] {
    const keysToFormat = ['date', 'link'];
    const formattedData = context.keys().map((key): T => {
      const content = { ...context(key) };
      if (!FormatterService.hasKeyToFormat(keysToFormat, content)) return content;
      if (content.date) content.date = FormatterService.formatDate(content.date);
      if (content.link) content.link = `/${resource}/${basename(key, '.json')}`;
      return content;
    });

    return formattedData;
  }

  public static formatContent<T>(content: any): T {
    const keysToFormat = ['date'];
    const formattedContent = { ...content };
    if (!FormatterService.hasKeyToFormat(keysToFormat, content)) return content;
    if (content.date) formattedContent.date = FormatterService.formatDate(content.date);
    return formattedContent;
  }
}
