import { FormatterService } from './formatter.service';

export class DataService {
  /**
   * Get data context based on **resource** property
   * using the webpack *require.context()* function.
   *
   * > **issue:** the webpack require.context() function only accepts
   * literals as parameters, so, variables are not allowed.
   * In order to work around this problem, a switch case is needed.
   *
   * > [webpack issue #4772](https://github.com/webpack/webpack/issues/4772)
   */
  private static getDataContext(resource: string): __WebpackModuleApi.RequireContext {
    switch (resource) {
      case 'palavras':
        return require.context('~/content/palavras', false,  /\.json$/);
      case 'eventos':
        return require.context('~/content/eventos', false,  /\.json$/);
      default:
        throw new Error(`[DataService]: Failed to fetch data. Invalid resource ${resource}`);
    }
  }

  private static getContentFromFolder<T>(resource: string): T[] {
    const context = this.getDataContext(resource);
    const formattedData = FormatterService.formatData<T>(context, resource);
    return formattedData;
  }

  public static loadFromFolder<T>(resource: string): T[] {
    const data = this.getContentFromFolder<T>(resource);
    return data;
  }

  public static loadFromFile<T>(resource: string, filename: string, list = false): T {
    const data: T = require(`~/content/${resource}/${filename}.json`);
    const content = list ? data[filename] : data;
    const formattedContent: T = FormatterService.formatContent(content);
    return formattedContent;
  }
}
