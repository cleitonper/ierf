import Vue from 'vue';

Vue.directive('scrolll', {
  inserted: (element, binding): void => {
    const onScroll = (event): void => {
      event.stopPropagation();
      if (binding.value(event, element)) {
        window.removeEventListener('scroll', onScroll);
      }
    };
    window.addEventListener('scroll', onScroll);
  },
});
