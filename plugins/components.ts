import Vue from 'vue';
import { Img, PageTitle, PageSubtitle, SectionTitle } from '~/components';

Vue.component('app-img', Img);
Vue.component('page-title', PageTitle);
Vue.component('page-subtitle', PageSubtitle);
Vue.component('section-title', SectionTitle);
