import Vue from 'vue';

Vue.directive('in-view', {
  inserted: (element, binding): void => {
    let beforeInsertClass = '';
    let insertedClass = '';
    const defaultClass = 'enter';
    const classPrefix = 'in-view';
    const argumentType = typeof binding.value;
    const allowedTypes = ['undefined', 'string', 'boolean', 'function'];

    if (!allowedTypes.includes(argumentType) || binding.value === false) {
      return;
    }

    if (argumentType === 'undefined' || binding.value === true) {
      beforeInsertClass = `${classPrefix}__will-${defaultClass}`;
    }

    if (argumentType === 'string') {
      beforeInsertClass = `${classPrefix}__will-${binding.value}`;
    }

    if (beforeInsertClass) {
      element.classList.add(beforeInsertClass);
    }

    const observer = new IntersectionObserver((entires) => {
      const target = entires[0];

      if (target.isIntersecting) {
        if (argumentType === 'undefined' || binding.value === true) {
          insertedClass = `${classPrefix}__${defaultClass}`;
        }

        if (argumentType === 'string') {
          insertedClass = `${classPrefix}__${binding.value}`;
        }

        if (argumentType === 'function') {
          binding.value(element);
        }

        if (beforeInsertClass && insertedClass) {
          element.classList.replace(
            beforeInsertClass,
            insertedClass,
          );

          setTimeout(() => element.classList.remove(insertedClass), 1500);
        }

        observer.disconnect();
      }
    });

    observer.observe(element);
  },
});
