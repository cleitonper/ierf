import { RootState } from '~/types/store';
import { MutationTree } from 'vuex';

const SITE_NAME = 'Igreja Evangélica Rompendo em Fé';

const initialState: RootState = {
  pageTitle: SITE_NAME,
};

export const state = (): RootState => initialState;

export const mutations: MutationTree<RootState> = {
  setPageTitle(store: RootState, pageTitle): void {
    store.pageTitle = `${SITE_NAME} - ${pageTitle}`;
  },
};
