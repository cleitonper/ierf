[![Logo](https://i.ibb.co/n7nwYVK/logo.png)](https://ierf.netlify.com)

# IERF

> Site da Igreja Evangélica Rompendo em Fé

[![Build Status](https://gitlab.com/cleitonper/ierf/badges/master/build.svg)](https://gitlab.com/cleitonper/ierf/pipelines)

## Funcionalidades 🧐

* Funcionamento offline
* Possibilidade de instalar o site como app
* Carregamento progressivo de imagens
* Geração automática de imagens responsivas
* Transições durante a navegação entre páginas
* Gerenciador de Conteúdo baseado em Git
* Geração de site estático (muito barato para hospedar)
* Análise Estática de Código
* Testes automatizados
* Integração Contínua
* Deploy Contínuo
* e mais...

## Produção 😌

Clique [`aqui`](https://ierf.netlify.com) e veja a aplicação sendo executada em *produção*.

## Desenvolvimento 🤓

``` bash
# instale as dependências do projeto
$ yarn install

# veja mudanças em localhost:3000
$ yarn run dev

# faça o build de produção e execute a aplicação
$ yarn run build
$ yarn start

# gere um site estático
$ yarn run generate
```

Para mais detalhes, acesse [Nuxt.js docs](https://nuxtjs.org).
