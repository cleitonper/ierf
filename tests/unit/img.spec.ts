import { basename } from 'path';
import { Wrapper, shallowMount } from '@vue/test-utils';
import Img from '~/components/img.vue';

describe('Component: Img', () => {
  let component: Wrapper<Img>;
  const src = 'path/to/my/img.png';
  const alt = 'My Image';
  const computedSrc = `/${basename(src)}`;

  beforeEach(() => {
    component = shallowMount(Img, {
      propsData: { src, alt },
      stubs: { NuxtImg: true },
    });
  });

  describe('Data[loaded]: false', () => {
    beforeEach(() => {
      component.setData({ loaded: false });
    });

    it('should render the placeholder image', () => {
      const imageQuery = '?style=placeholder';
      const placeholder = component.find('.placeholder');
      expect(placeholder.attributes('src')).toBe(`${computedSrc}${imageQuery}`);
      expect(placeholder.attributes('alt')).toBe(alt);
    });

    it('should not render the full image', () => {
      expect(component.contains('.image')).toBeFalsy();
    });
  });

  describe('Data[loaded]: true', () => {
    beforeEach(() => {
      component.setData({ loaded: true });
    });

    it('should hide the placeholder image', () => {
      component.setData({ showPlaceholder: false });
      expect(component.find('.placeholder').classes('hide')).toBeTruthy();
    });

    it('should render the full image', () => {
      const image = component.find('.image');
      expect(image.attributes('src')).toBe(computedSrc);
      expect(image.attributes('alt')).toBe(alt);
    });
  });
});
