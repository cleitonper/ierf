import { Wrapper, shallowMount } from '@vue/test-utils';
import Navigation from '~/components/navigation.vue';
import { NavigationItem } from '~/types';

describe('Component: Navigation', () => {
  let component: Wrapper<Navigation>;

  const items: NavigationItem[] = [
    { link: '/home', icon: 'home', title: 'Home' },
    { link: '/about', icon: 'question', title: 'About' },
  ];

  beforeEach(() => {
    component = shallowMount<Navigation>(Navigation);
  });

  describe('Prop[items]: empty', () => {
    it('should not render navigation items', () => {
      expect(component.findAll('.navigation-link')).toHaveLength(0);
    });
  });

  describe('Prop[items]: filled', () => {
    beforeEach(() => {
      component.setProps({ items });
    });

    it('should render navigation items', () => {
      expect(component.findAll('.navigation-link')).toHaveLength(items.length);
    });

    it('should render navigation item', () => {
      const lastIndex = items.length;
      const index = Math.floor(Math.random() * lastIndex);
      const item = items[index];
      const renderedItem = component.findAll('.navigation-link').at(index);
      expect(renderedItem.find('.title').text()).toBe(item.title);
      expect(renderedItem.find('.icon').text()).toBe(item.icon);
    });
  });
});
