import { shallowMount, Wrapper } from '@vue/test-utils';
import Header from '~/components/header.vue';
import { NavigationItem } from '~/types';

describe('Component: Header', () => {
  let component: Wrapper<Header>;
  const items: NavigationItem[] = [
    { title: 'First', link: '#' },
    { title: 'Scond', link: '#' },
  ];

  it('shoud emmit toggleMenu event when hamburger component is clicked', () => {
    component = shallowMount<Header>(Header, { stubs: ['nuxt-link'] });
    component.find('hamburger-stub').trigger('click');
    expect(component.emitted().toggleMenu).toBeTruthy();
  });

  describe('Prop[items]: empty', () => {
    beforeAll(() => {
      component = shallowMount<Header>(Header, { stubs: ['nuxt-link'] });
    });

    it('should not render the navigation bar', () => {
      expect(component.find('.navigation').exists()).toBeFalsy();
    });
  });

  describe('Prop[items]: filled', () => {
    beforeAll(() => {
      component = shallowMount<Header>(Header, { stubs: ['nuxt-link'] });
      component.setProps({ items });
    });

    it('should render navigation items', () => {
      const navigation = component.find('.navigation');
      expect(navigation.exists()).toBeTruthy();
      expect(navigation.findAll('.item')).toHaveLength(items.length);
    });

    it('should render navigation item content', () => {
      const index = items.length - 1;
      const item = items[index];
      const renderedItem = component.findAll('.item').at(index);
      expect(renderedItem.text()).toBe(item.title);
    });
  });

  describe('Sticky: disabled', () => {
    beforeAll(() => {
      component = shallowMount<Header>(Header, { stubs: ['nuxt-link'] });
      component.setProps({ sticky: false });
    });

    it('should enable flat mode', () => {
      expect(component.attributes('flat')).toBeTruthy();
    });

    it('should set colot to transparent', () => {
      expect(component.attributes('color')).toBe('transparent');
    });

    it('should set height to 80', () => {
      expect(component.attributes('height')).toBe('80');
    });
  });

  describe('Sticky: enabled', () => {
    beforeAll(() => {
      component = shallowMount<Header>(Header, { stubs: ['nuxt-link'] });
      component.setProps({ sticky: true });
    });

    it('should disable flat mode', () => {
      expect(component.attributes('flat')).toBeFalsy();
    });

    it('should set color to white', () => {
      expect(component.attributes('color')).toBe('#fff');
    });

    it('should set height to 60', () => {
      expect(component.attributes('height')).toBe('60');
    });
  });
});
