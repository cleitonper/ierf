import { shallowMount, Wrapper } from '@vue/test-utils';
import Featured from '~/components/featured.vue';
import { Post } from '~/types';

describe('Component: Featured', () => {
  let component: Wrapper<Featured>;
  const items: Post[] = [
    { title: 'T1', intro: 'I1', content: 'C1', image: '/cat.png', link: '/cat', date: '2019-02-01' },
    { title: 'T2', intro: 'I2', content: 'C2', image: '/dog.png', link: '/dog', date: '2019-02-01' },
  ];

  describe('Prop[items]: empty', () => {
    beforeAll(() => {
      component = shallowMount<Featured>(Featured);
    });

    it('should not render the featured list', () => {
      expect(component.find('.featured').exists()).toBeFalsy();
    });
  });

  describe('Prop[items]: filled', () => {
    beforeAll(() => {
      component = shallowMount<Featured>(Featured);
      component.setProps({ items });
    });

    it('should render items', () => {
      expect(component.findAll('.item')).toHaveLength(items.length);
    });

    it('should render item content', () => {
      const index = items.length - 1;
      const item = items[index];
      const renderedItem = component.findAll('.item').at(index);
      expect(renderedItem.find('.image').attributes('src')).toEqual(item.image);
      expect(renderedItem.find('.title').text()).toEqual(item.title);
      expect(renderedItem.find('.content').text()).toEqual(item.intro);
    });
  });
});
