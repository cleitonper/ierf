import { shallowMount, Wrapper } from '@vue/test-utils';
import BtnUber from '~/components/btn-uber.vue';

describe('Component: BtnUber', () => {
  let component: Wrapper<BtnUber>;

  it('should have href attribute', () => {
    const uberLinkAddress = 'https://uber.com/address';
    component = shallowMount<BtnUber>(BtnUber);
    component.setData({ link: uberLinkAddress });
    expect(component.attributes('href')).toBe(uberLinkAddress);
  });
});
