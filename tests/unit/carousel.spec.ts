import { shallowMount, Wrapper } from '@vue/test-utils';
import Carousel from '~/components/carousel.vue';
import { CarouselItem } from '~/types';

describe('Component: Carousel', () => {
  let component: Wrapper<Carousel>;
  const items: CarouselItem[] = [
    { image: '/cat.jpg', content: 'Cat', detail: 'supercat' },
    { image: '/dog.jpg', content: 'Dog', detail: 'superdog' },
  ];

  describe('Prop[items]: empty', () => {
    beforeAll(() => {
      component = shallowMount<Carousel>(Carousel);
    });

    it('shoudl not render the carousel', () => {
      expect(component.find('.carousel').exists()).toBeFalsy();
    });
  });

  describe('Prop[items]: filled', () => {
    beforeAll(() => {
      component = shallowMount<Carousel>(Carousel);
      component.setProps({ items });
    });

    it('should render items', () => {
      const carousel = component.find('.carousel');
      expect(carousel.exists()).toBeTruthy();
      expect(carousel.findAll('.item')).toHaveLength(items.length);
    });

    it('should render item content', () => {
      const index = items.length - 1;
      const item = items[index];
      const renderedItem = component.findAll('.carousel .item').at(index);
      expect(renderedItem.find('.image').attributes('src')).toBe(item.image);
      expect(renderedItem.find('.content').text()).toBe(item.content);
      expect(renderedItem.find('.detail').text()).toBe(item.detail);
    });
  });
});
