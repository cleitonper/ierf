import { shallowMount, Wrapper } from '@vue/test-utils';
import Banner from '~/components/banner.vue';
import { BannerContent } from '~/types';

describe('Component: Banner', () => {
  let component: Wrapper<Banner>;
  const content: BannerContent = {
    title: 'My Banner',
    description: 'My content',
    image: '/image/nyancat.jpg',
    call2action: 'Read more',
    link: '/awesomecat',
  };

  describe('Prop[content]: empty', () => {
    beforeAll(() => {
      component = shallowMount<Banner>(Banner);
    });

    it('should not render the banner', () => {
      expect(component.contains('.banner')).toBeFalsy();
    });
  });

  describe('Prop[content]: filled', () => {
    beforeAll(() => {
      component = shallowMount<Banner>(Banner);
      component.setProps({ content });
    });

    it('should render banner content', () => {
      const banner = component.find('.banner');
      expect(banner.exists()).toBeTruthy();
      expect(banner.find('.image').attributes('src')).toEqual(content.image);
      expect(banner.find('.title').text()).toEqual(content.title);
      expect(banner.find('.description').text()).toEqual(content.description);
      expect(banner.find('.call2action').text()).toEqual(content.call2action);
    });
  });
});
