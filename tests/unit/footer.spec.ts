import { Wrapper, shallowMount } from '@vue/test-utils';
import Footer from '~/components/footer.vue';

describe('Component: Footer', () => {
  let component: Wrapper<Footer>;

  const socialNetworks = [
    { href: 'javascript:void', label: 'Facebook', icon: 'mdi-facebook' },
    { href: 'javascript:void', label: 'Instagram', icon: 'mdi-instagram' },
  ];

  it('should render social network links', () => {
    component = shallowMount<Footer>(Footer);
    component.setData({ contacts: socialNetworks });
    const renderedSocialNetworks = component.findAll('.social .item');
    expect(renderedSocialNetworks).toHaveLength(socialNetworks.length);
    expect(renderedSocialNetworks.is('a')).toBeTruthy();
  });
});
