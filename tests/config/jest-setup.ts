import Vuetify from 'vuetify';
import Vue from 'vue';

Vue.use(Vuetify);
Vue.directive('in-view', {});

Vue.config.productionTip = false;
