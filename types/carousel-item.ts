export interface CarouselItem {
  image: string;
  content: string;
  detail: string;
}
