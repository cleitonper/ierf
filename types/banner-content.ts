export interface BannerContent {
  image: string;
  title: string;
  description: string;
  call2action: string;
  link: string;
}
