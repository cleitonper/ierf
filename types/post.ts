export interface Post {
  image: string;
  date: string;
  title: string;
  intro: string;
  content: string;
  link?: string;
  author?: string;
}
