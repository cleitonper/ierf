export interface Schedule {
  day: string;
  begin: string;
  end: string;
}
