/*
  eslint
  @typescript-eslint/camelcase: "off",
  @typescript-eslint/no-use-before-define: "off"
*/

import glob from 'glob';
import path from 'path';

const { site } = require('./content/config/site.json');

const dynamicRoutes = getDynamicPaths({
  palavras: 'content/palavras/*.json',
  eventos:  'content/eventos/*.json',
});

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: site.name,
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'theme-color',
        content: '#ffffff',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: site.description,
      },
      {
        name: 'google-site-verification',
        content: '__SmmYbDQ0eCf1x41Y1Uxr7iJGnDJImacGOfGVIpQt4',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    'vuetify/dist/vuetify.min.css',
    '~/assets/style/global.css',
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    '@/plugins/scrolll',
    '@/plugins/in-view',
    '@/plugins/components',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    ['@reallifedigital/nuxt-image-loader-module', {
      imagesBaseDir: 'static/image/upload',
      imageStyles: {
        placeholder: { actions: ['quality|50'], macros: ['scaleAndCrop|80|45'] },
        small:       { actions: ['quality|80'], macros: ['scaleAndCrop|640|360'] },
        medium:      { actions: ['quality|80'], macros: ['scaleAndCrop|960|540'] },
        large:       { actions: ['quality|80'], macros: ['scaleAndCrop|1200|675'] },
      },
      responsiveStyles: {
        thumb: {
          srcset: 'small 600w, medium 960w, large 1200w',
          sizes: '(max-width: 600px) 600px, (max-width: 960px) 960px, (min-width: 961px) 1200px',
        },
      },
      forceGenerateImages: {
        placeholder: '**/*',
        small: '**/*',
        medium: '**/*',
        large: '**/*',
      },
    }],
    ['@nuxtjs/google-tag-manager', { id: 'GTM-TKFRKSJ' }],
    '@nuxtjs/pwa',
    '@nuxtjs/markdownit',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
  ],

  /*
  ** Static generation configuration
  */
  generate: {
    routes: dynamicRoutes,
  },

  /*
  ** sitemap.xml configuration
  */
  sitemap: {
    hostname: site.address,
    exclude: ['/admin/**'],
  },

  robots: {
    UserAgent: '*',
    Disallow: '/admin',
    Sitemap: `${site.address}/sitemap.xml`,
  },

  /*
  ** @nuxt/pwa manifest configuration
  */
  manifest: {
    name: site.name,
    short_name: site.short_name,
    description: site.description,
    lang: site.lang,
  },

  /*
  ** @nuxt/pwa workbox configuration
  */
  workbox: {
    runtimeCaching: [
      {
        urlPattern: '/image-styles/.*',
        handler: 'cacheFirst',
        strategyOptions: {
          cacheName: 'image-cache',
          cacheExpiration: {
            maxEntries: 100,
            maxAgeSeconds: 604800,
          },
        },
      },
    ],
  },

  /*
  ** Markdownit configuration
  */
  markdownit: {
    injected: true,
    preset: 'zero',
    breaks: true,
  },

  /*
  ** Build configuration
  */
  build: {
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value
        'autoprefixer': {},
        'postcss-nested': {},
        'postcss-import': {},
        'postcss-mixins': { mixinsDir: './assets/style/mixins' },
      },
    },
  },
};

/**
 * Create an array of URLs from a list of files
 * @param {object} urlFilepathMap
 */
function getDynamicPaths(urlFilepathMap): string[] {
  const intermediateUrls = Object.keys(urlFilepathMap);
  const urls: string[] = [];

  return urls.concat(
    ...intermediateUrls.map((resource) => {
      const filepathGlob = urlFilepathMap[resource];
      const routes =  glob
        .sync(filepathGlob)
        .map((filename) => `/${resource}/${path.basename(filename, '.json')}`);
      return routes;
    }),
  );
}
